window.onload = ()=>{
    // selects
    const charAmountRange = document.querySelector('#charAmountRange')
    const charAmountNumber = document.querySelector('#charAmountNumber')
    const form = document.querySelector('#passwordGenerator')
    const includeUppercaseElement = document.querySelector('#includeUppercase')
    const includeNumbersElement = document.querySelector('#includeNumbers')
    const includeSymbolsElement = document.querySelector('#includeSymbols')
    const display = document.querySelector('#display')

    //variables 
    const LOWERCASE_CHAR_CODES = arryFromLowToHigh(97, 122)
    const UPPERCASE_CHAR_CODES = arryFromLowToHigh(65, 90)
    const NUMBER_CHAR_CODES = arryFromLowToHigh(48, 57)
    const SYMBOL_CHAR_CODES = arryFromLowToHigh(9, 47)
                             .concat(arryFromLowToHigh(58, 64)
                             .concat(arryFromLowToHigh(91, 96)
                             .concat(arryFromLowToHigh(123, 126))))

    const syncCharAmount = (event)=> {
        const value = event.target.value
        charAmountNumber.value = value
        charAmountRange.value = value
    }

    // password generate function

    const genaratePassword = (chrAmount, includeUppercase, 
    includeNumbers, includeSymbols)=> {
        let charCodes = LOWERCASE_CHAR_CODES
        if (includeUppercase) charCodes = charCodes.concat(UPPERCASE_CHAR_CODES)
        if (includeSymbols) charCodes = charCodes.concat(SYMBOL_CHAR_CODES)
        if (includeNumbers) charCodes = charCodes.concat(NUMBER_CHAR_CODES)
        const passwordChars = []
        for (let i = 0; i < chrAmount; i++) {
            const charCode = charCodes[Math.floor(Math.random()*charCodes.length)]
            passwordChars.push(String.fromCharCode(charCode))
        }
        return passwordChars.join('')
    }

    function arryFromLowToHigh(Low, High){
        const array = []
        for (let i = Low; i <= High; i++) {
            array.push(i)
        }
        return array
    }

    // events
    charAmountRange.addEventListener('input', syncCharAmount)
    charAmountNumber.addEventListener('input', syncCharAmount)

    form.addEventListener('submit', (event)=> {
        event.preventDefault()
        const chrAmount = charAmountNumber.value
        const includeUppercase = includeUppercaseElement.checked
        const includeNumbers = includeNumbersElement.checked
        const includeSymbols = includeSymbolsElement.checked
        const password = genaratePassword(chrAmount, includeUppercase, 
        includeNumbers, includeSymbols)
        display.innerText = password
    })
}